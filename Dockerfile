FROM nginx:1.15.9
EXPOSE 80

COPY ./nginx/conf.d /etc/nginx/conf.d
WORKDIR /usr/src/app

# Install python 3
RUN apt-get update -y && \
apt-get install -y python3-pip python3-dev build-essential && \
pip3 install --upgrade pip


COPY . .
RUN pip install -r service_init/requirements.txt
RUN pip install -r search_rest/requirements.txt

# Path for example pickles and models
ENV DATA_PATH /usr/src/app/data
# Path for running elasticsearch
ENV PYTHONUNBUFFERED 1
ENV FLASK_APP search_rest/app
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV ES_HOST localhost
ENV ES_PORT 9200

CMD bash /usr/src/app/docker-entrypoint.sh