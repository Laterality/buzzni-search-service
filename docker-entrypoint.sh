#! /usr/bin/env

echo "Run nginx in background"
# Run nginx daemon in background
nginx

echo "Run ES initializer"
# Run ES initializing app
python3 service_init/app.py

# Run REST API app
flask run --host 0.0.0.0