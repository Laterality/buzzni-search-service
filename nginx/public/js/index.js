const ctx = {
    constants: {
        API_HOST: "http://localhost/api",
        URL_PREFIX: "http://hsmoa.com"
    },
    view: {
        getCheckedCategoryElements: () => {
            return $("input[name=cat]:checked");
        },
        getCheckedShopHostElements: () => {
            return $("input[name=shop_host]:checked");
        },
        getKeywordValue: () => {
            return $("#input-name").val();
        },
        getMinPriceValue: () => {
            return $("#input-price-min").val();
        },
        getMaxPriceValue: () => {
            return $("#input-price-max").val();
        },
        getAlphaValue: () => {
            return $("#input-alpha").val();
        },
        setResultHTML: (html) => {
            $(".results")[0].innerHTML = html;
        }
    },
    util: {
        getCheckedCategoryNames: () => {
            const elms = ctx.view.getCheckedCategoryElements();
            const catNames = [];
            for (i of elms) {
                catNames.push(i.value);
            }

            return catNames;
        },
        getCheckedShopHostNames: () => {
            const elms = ctx.view.getCheckedShopHostElements();
            const shNames = [];
            for (i of elms) {
                shNames.push(i.value);
            }
            
            return shNames;
        },
        createQueryString: (query) => {
            let str = "";
            let isFirstKey = true;

            if (query["keyword"].length > 0) {
                str += `${isFirstKey? "?" : ""}q=${query["keyword"]}`;
                if (isFirstKey) { isFirstKey = false; }
            }

            if (query["minPrice"].length > 0) {
                str += `${isFirstKey? "?" : "&"}price_min=${query["minPrice"]}`;
                if (isFirstKey) { isFirstKey = false; } 
            }
            
            if (query["maxPrice"].length > 0) {
                str += `${isFirstKey? "?" : "&"}price_max=${query["maxPrice"]}`;
                if (isFirstKey) { isFirstKey = false; }
            }

            if (query["cat"].length > 0) {
                str += `${isFirstKey? "?" : "&"}cat=${query["cat"].join(",")}`;
                if (isFirstKey) { isFirstKey = false; }
            }

            if (query["shop_host"].length > 0) {
                str += `${isFirstKey? "?" : "&"}shop_host=${query["shop_host"].join(",")}`;
                if (isFirstKey) { isFirstKey = false; }
            }

            str += `${isFirstKey? "?" : "&"}alpha=${query["alpha"].length > 0? query["alpha"] : 1}`;

            return str;
        },
        searchRequest: (qs, cb) => {
            const xhr = new XMLHttpRequest();
            xhr.addEventListener("load", () => { cb(xhr); });
            xhr.open("GET", ctx.constants.API_HOST + "/search" + qs);
            xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
            xhr.send();
        }
    },
    eventHandler: {
        onSearchClicked: (evt) => {
            const keyword = ctx.view.getKeywordValue();
            const minPrice = ctx.view.getMinPriceValue();
            const maxPrice = ctx.view.getMaxPriceValue();
            const catNames = ctx.util.getCheckedCategoryNames();
            const shNames = ctx.util.getCheckedShopHostNames();
            const alpha = ctx.view.getAlphaValue();

            const query = {
                keyword,
                minPrice,
                maxPrice,
                "cat": catNames,
                "shop_host": shNames,
                alpha,
            };

            const qs = ctx.util.createQueryString(query);

            console.log(qs);
            ctx.util.searchRequest(qs, (xhr) => {
                const body = JSON.parse(xhr.response);
                console.log(body);

                ctx.view.setResultHTML(ctx.template.createSearchResultList(body["data"]));
            });
        }
    },
    template: {
        createSearchResultItem: (item) => {
            const name = item["_source"]["name"];
            const cat = item["_source"]["cat"];
            const price = item["_source"]["price"];
            const score = item["_score"];
            const url = item["_source"]["url"];
            return `
            <li class="list-group-item"><div>
                <a href="${ctx.constants.URL_PREFIX}${url}"><h3 class="d-inline-block">${name}</h3></a> <strong>${cat}</strong>
                <h6>${price}원</h6>
                <h6>score: ${score}</h6>
            </div></li>
            `;
        },
        createSearchResultList: (items) => {
            let itemsHTML = items.map((obj, idx) => ctx.template.createSearchResultItem(obj) );

            return `
            <ul class="list-group-flush">
            ${itemsHTML.join("")}
            </ul>
            `;
        }
    }
}

document.addEventListener("DOMContentLoaded", () => {
    $("#button-search").click(ctx.eventHandler.onSearchClicked);
});