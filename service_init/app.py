import datetime
import time
import numpy as np
import requests
import os
from multiprocessing import Pool
from multiprocessing import cpu_count

from crawler import requester
from crawler import parser
from classifier import classifier as cs
from es_sender import sender
from es_init import initializer


ES_HOST = "localhost"
ES_PORT = 9200
DATA_PATH = ""
RETRY_INTERVAL = 5  # Interval between retries when ES node doesn't respond

if "ES_HOST" in os.environ:
    ES_HOST = os.environ["ES_HOST"]

if "ES_PORT" in os.environ:
    ES_PORT = int(os.environ["ES_PORT"])

if "DATA_PATH" in os.environ:
    DATA_PATH = os.environ["DATA_PATH"]


def make_date_list():
    sec_per_day = 24 * 60 * 60
    now_sec = time.time()

    date_strs = [datetime.datetime.fromtimestamp(now_sec).strftime("%Y%m%d")]
    for i in range(1, 31):
        timestamp = now_sec - (sec_per_day * i)
        date = datetime.datetime.fromtimestamp(timestamp)
        date_strs.append(date.strftime("%Y%m%d"))
    return date_strs


def crawl(datestr):
    text = requester.Requester.request(datestr)
    contents = parser.Parser.parse(text)
    return datestr, contents


def crawl_recent_month():
    dates = make_date_list()
    pool = Pool(cpu_count() * 2)

    start = time.time()
    results = pool.map(crawl, dates)

    classifier = cs.Classifier(DATA_PATH)
    local_sender = sender.Sender("http://%s:%d" % (ES_HOST, ES_PORT))

    # result => (datestr, contents)
    for result in results:
        print("%s: %d" % (result[0], len(result[1])))

        for i in result[1]:
            i.cat = classifier.convert_to_cate(np.argmax(classifier.predict(i.name)))

        local_sender.post_contents(result[0], result[1])

    elapsed = time.time() - start
    print("Finished in %ds" % elapsed)


def wait_es_run():
    while True:
        try:
            res = requests.get("http://%s:%d" % (ES_HOST, ES_PORT))
            if res.status_code == 200:
                break
            time.sleep(RETRY_INTERVAL)
        except requests.exceptions.ConnectionError:
            pass


if __name__ == "__main__":
    """
    Initialize Elasticsearch(index template, script, etc.) and Crawl the site and index them.
    """

    print("Waiting for ES")
    wait_es_run()
    print("ES running now!")
    initializer.ES_Initializer(ES_HOST, ES_PORT, DATA_PATH).initialize()
    crawl_recent_month()
