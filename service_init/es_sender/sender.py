import elasticsearch
import elasticsearch.helpers


class Sender:

    def __init__(self, es_host):
        self.es_conn = elasticsearch.Elasticsearch(es_host)

    def generate(self, datestr, contents):
        for i in contents:
            yield {
                "_index": "scrape-%s" % datestr,
                "_type": "_doc",
                "name": i.name,
                "price": i.price,
                "url": i.url,
                "shop_host": i.shop_host,
                "cat": i.cat,
            }

    def post_contents(self, datestr, contents):
        """
        Send collected objects to ES.

        :param contents es로 보낼 Product 객체 리스트
        """
        elasticsearch.helpers.bulk(self.es_conn, self.generate(datestr, contents))
