import bs4
import re

from . import product


class Parser:

    @staticmethod
    def refine_price(price):
        """
        Convert price string to int value
        ex. 299,000원 → 299000
        """
        if price == "" or price == 0:
            return 0
        return int("".join(re.compile("[,원]").split(price)))

    @staticmethod
    def parse(content):
        products = []
        soup = bs4.BeautifulSoup(content, "html.parser")
        items = soup.select(".timeline-item")

        for item in items:
            # print(item["class"])
            shop_host = item["class"][1]

            # print(shop_host)

            disblocks = item.select(".disblock")

            prod = Parser.create_product(disblocks[0], shop_host, False)
            if prod is None:
                continue
            products.append(prod)
            for disblock in disblocks[1:]:
                p = Parser.create_product(disblock, shop_host, True)
                if p is None:
                    continue
                products.append(p)

        print("Parsed %d products" % len(products))
        return products

    @staticmethod
    def create_product(item, shop_host, same_time):
        """
        create Product object with "a.disblock" element

        :return Created product object, None if item is not a product
        """
        url = item["href"]
        cell = item.select(".display-table > .table-cell:nth-child(3)")[0]

        if not same_time:
            name = cell.select(".font-15")[0].get_text().strip()
            price = cell.select(".font-17")
            if len(price) != 0:
                price = price[0].get_text()
            else:
                price = 0
        else:
            name = cell.select(".font-13")[0].get_text().strip()
            price = cell.select(".font-14")
            if len(price) != 0:
                price = price[0].get_text()
            else:
                price = 0

        if "지금은 공익방송 시간입니다." in name:
            return None

        price = Parser.refine_price(price)
        return product.Product(
            name=name,
            shop_host=shop_host,
            price=price,
            url=url
        )
