import requests


class Requester:

    @staticmethod
    def request(datestr):
        url = "http://hsmoa.com?date=%s" % datestr

        r = requests.get(url=url)
        return r.text
