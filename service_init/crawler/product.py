class Product:

    def __init__(self, name, shop_host, price, url):
        self.name = name
        self.shop_host = shop_host
        self.price = price
        self.url = url
        self.cat = None

    def __str__(self):
        return "{name: %s, shop_host: %s, price: %d, url: %s, cat: %s}" % \
               (self.name, self.shop_host, self.price, self.url, self.cat)
