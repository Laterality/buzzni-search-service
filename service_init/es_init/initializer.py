import json
import requests
import os


class ES_Initializer:

    def __init__(self, es_host, es_port, data_path):
        self.host = es_host
        self.port = es_port
        self.data_path = data_path

    def _get_es_address(self):
        return "http://%s:%d" % (self.host, self.port)

    def _add_index_templates(self):
        with(open(os.path.join(self.data_path, "index-templates.json"), "r", encoding="utf-8")) as f:
            obj = json.load(f)
            print("Got %d templates." % len(obj))
            for i in obj:
                requests.put("%s/_template/%s" % (self._get_es_address(), i), json=obj[i])

    def _add_scripts(self):
        with(open(os.path.join(self.data_path, "scripts.json"), encoding="utf-8")) as f:
            obj = json.load(f)
            print("Got %d scripts." % len(obj))
            for i in obj:
                requests.post("%s/_scripts/%s" % (self._get_es_address(), i), json=obj[i])

    def _add_rank_stat(self):
        with(open(os.path.join(self.data_path, "rank-stat-converted.json"), encoding="utf-8")) as f:
            obj = json.load(f)
            print("Got %d stats." % len(obj))
            for i in obj:
                requests.post("%s/rank-stat/_doc" % self._get_es_address(), json=i)

    def initialize(self):
        self._add_index_templates()
        self._add_scripts()
        self._add_rank_stat()
        print("ES initialized")

