class ScrapeQueryBuilder:

    def __init__(self):
        self.match_all = True
        self.name = None
        self.shop_hosts = []
        self.cats = []
        self.price_min = -1
        self.price_max = -1
        self.alpha = 1.0
        self.query_stat = None
        self.sorts = []

    def _create_match(self, field, value):
        return {"match": {field: value}}

    def _create_terms(self, field, values):
        return {"terms": {field: values}}

    def with_keyword(self, keyword):
        self.name = keyword
        return self

    def with_shop_host(self, shop_host):
        if shop_host is not None:
            self.shop_hosts.append(shop_host)
        return self

    def with_category(self, cat):
        if cat is not None:
            self.cats.append(cat)
        return self

    def with_price_min(self, price_min):
        self.price_min = price_min
        return self

    def with_price_max(self, price_max):
        self.price_max = price_max
        return self

    def with_stat(self, stat):
        """
        with_stat sets query stat to query
        :param stat: dictionary for query stat
            ex. {
                    "category_1": 0.8,
                    "category_2": 0.2,
                }
        """
        self.query_stat = stat
        return self

    def with_alpha(self, alpha):
        if alpha is not None:
            self.alpha = alpha
        return self

    def sort(self, field, order):
        """
        Set query with sort order of specified field
        :param field: Name of field to sort
        :param order: Sort order ["asc", "desc"]
        """
        if field is not None and order is not None:
            self.sorts.append({field: order})

    def build(self):
        query = {}

        """
        Build match query
        """
        query["matches"] = []
        if self.name is not None:
            query["matches"].append(self._create_match("name", self.name))
        if len(self.shop_hosts) > 0:
            query["matches"].append(self._create_terms("shop_host", self.shop_hosts))
        if len(self.cats) > 0:
            query["matches"].append(self._create_terms("cat", self.cats))

        if len(query["matches"]) == 0:
            del query["matches"]
            query["match_all"] = True

        """
        Build price range query
        """
        if self.price_min >= 0 or self.price_max >= 0:
            query["price"] = {}

            if self.price_min >= 0:
                query["price"]["min"] = self.price_min
            if self.price_max >= 0:
                query["price"]["max"] = self.price_max

        """
        Build query stat parameter
        """
        query["alpha"] = self.alpha
        if self.query_stat is None:
            query["stats"] = {}
        else:
            query["stats"] = self.query_stat

        """
        Build sort
        """
        if len(self.sorts) > 0:
            query["sorts"] = self.sorts

        return query
