import elasticsearch

from .query_builder import ScrapeQueryBuilder


class ES_Searcher:

    def __init__(self, es_host, es_port):
        self.es = elasticsearch.Elasticsearch("%s:%d" % (es_host, es_port))

    def _search_query_stat(self, keyword):
        result = self.es.search_template(
            body={
                "id": "search-rank-stat",
                "params": {
                    "keyword": keyword,
                },
            })

        if len(result["hits"]["hits"]) == 0:
            return {}
        else:
            return result["hits"]["hits"][0]["_source"]["stats"]

    def search_scrape(self, keyword=None, shop_host=None, cat=None, price_min=-1, price_max=-1, sorts=[], alpha=1.0):
        """
        :param keyword:
        :param shop_host:
        :param cat:
        :param price_min:
        :param price_max:
        :param sorts: Array of tuples specifying fields to sort ex. ("price": "asc")
        :param alpha: alpha variable which is used in scoring
        :return:
        """
        qb = ScrapeQueryBuilder()

        if keyword is not None:
            """
            Retrieve query stat.
            """
            stat = self._search_query_stat(keyword)
            qb.with_stat(stat)

        qb.with_keyword(keyword=keyword) \
            .with_price_min(price_min=price_min) \
            .with_price_max(price_max=price_max) \
            .with_alpha(alpha=alpha)

        if cat is not None:
            for c in cat:
                qb.with_category(c)

        if shop_host is not None:
            for s in shop_host:
                qb.with_shop_host(s)

        for s in sorts:
            if s is not None:
                qb.sort(s[0], s[1])

        params = qb.build()
        print(params)
        result = self.es.search_template(
            index="scrape-*",
            body={
                "id": "search-scrape",
                "params": params,
            })
        return result["hits"]["hits"]


if __name__ == '__main__':
    searcher = ES_Searcher("localhost", 9200)
    result = searcher.search_scrape(price_min=20000, price_max=40000, sorts=[("price", "desc")])
    print(result)
