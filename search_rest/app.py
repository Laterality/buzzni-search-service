import os
import flask
import flask_cors

from .es_searcher import ES_Searcher

ES_HOST = "localhost"
ES_PORT = 9200

if "ES_HOST" in os.environ:
    ES_HOST = os.environ["ES_HOST"]

if "ES_PORT" in os.environ:
    ES_PORT = int(os.environ["ES_PORT"])

searcher = ES_Searcher(ES_HOST, ES_PORT)
app = flask.Flask(__name__)
flask_cors.CORS(app)


def parse_sort_string(str):
    """
    Convert query string for sorting results to format for query
    price:asc → (price, "asc")
    :param str:
    :return:
    """
    tokens = str.split(":")

    return tokens[0], tokens[1]


def parse_cat_string(str):
    """
    Convert query string for category range to format for query
    cat1,cat2 → ["cat1", "cat2"]
    :param str:
    :return:
    """
    return str.split(",")


@app.route("/search", methods=["GET"])
def search():
    args = flask.request.args

    if "q" in args:
        keyword = args["q"]
    else:
        keyword = None

    if "shop_host" in args:
        shop_host = parse_cat_string(args["shop_host"])
    else:
        shop_host = None

    if "cat" in args:
        cat = parse_cat_string(args["cat"])
    else:
        cat = None

    if "price_min" in args:
        price_min = int(args["price_min"])
    else:
        price_min = -1

    if "price_max" in args:
        price_max = int(args["price_max"])
    else:
        price_max = -1

    if "sort" in args:
        sort = parse_sort_string(args["sort"])
    else:
        sort = None

    if "alpha" in args:
        alpha = float(args["alpha"])
    else:
        alpha = 1.0

    result = searcher.search_scrape(keyword=keyword, shop_host=shop_host, cat=cat,
                                    price_min=price_min, price_max=price_max, sorts=[sort], alpha=alpha)

    return flask.jsonify({
        "result": "ok",
        "data": result
    })

